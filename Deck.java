import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		String[] suits = new String[]{"hearts", "spades", "clubs", "diamond"};
		String[] values = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"};
		this.rng = new Random();
		this.numberOfCards = 52;
		this.cards = new Card[52];
		
		int a = 0;
		for(int i = 0; i < suits.length;i++){
			for(int j = 0; j < values.length;j++){
				this.cards[a] = new Card(suits[i], values[j]);
				a++;
			}
		}
	}
	public int length(){
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		Card topCard = this.cards[this.numberOfCards-1];
		this.numberOfCards--;
		return topCard;
		
}	
	public String toString(){
		String result = "";
		for(int i = 0; i < numberOfCards; i++){
			result += this.cards[i] + "\n";
		}
		return result;

		}
	public void shuffle(){
		for(int i = 0; i < numberOfCards; i++){
			int pos = this.rng.nextInt(i + 1);
			Card temp = cards[i];
			this.cards[i] = this.cards[pos];
			this.cards[pos] = temp;
		}
	}

}