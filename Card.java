public class Card{
	private String suit;
	private String value;
	
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	public String getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.value + " of " + this.suit;
	}
	public double calculateScore(){
		double score = 0;
		score = Double.parseDouble(getValue());
		if(getSuit().equals("hearts")){
			score += 0.4;
		}
		else if(getSuit().equals("diamond")){
			score += 0.3;
		}
		else if(getSuit().equals("clubs")){
			score += 0.2;
		}
		else{
			score += 0.1;
		}
		return score;
	}
}